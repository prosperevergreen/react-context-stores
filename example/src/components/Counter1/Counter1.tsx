import React from 'react';
import Counter from '../Counter/Counter';
import { contextStore } from '../../App';

const Counter1 = () => {
  const { counter1, setCounter1 } = contextStore.useStore();
  return (
    <Counter
      title={'Context Store Example'}
      data={counter1}
      increase={() => setCounter1(counter1 + 1)}
      reset={() => setCounter1(0)}
      decrease={() => setCounter1(counter1 - 1)}
    />
  );
};

export default Counter1;
