import React from 'react';
import { contextCombineStore } from '../../App';
import { actions } from '../../store/counterSlice';
import Counter from '../Counter/Counter';

const Counter3 = () => {
  const { counter3Store } = contextCombineStore.useStore();
  const [{ counter: counter3 }, counter3Dispatch] = counter3Store;
  return (
    <Counter
      title={'Context Combine Store Example 1'}
      data={counter3}
      increase={() => counter3Dispatch(actions.change(-1))}
      reset={() => counter3Dispatch(actions.reset)}
      decrease={() => counter3Dispatch(actions.change(-1))}
    />
  );
};

export default Counter3;
