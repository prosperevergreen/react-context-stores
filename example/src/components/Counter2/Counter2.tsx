import React from 'react';
import { contextReduxStore } from '../../App';
import { actions } from '../../store/counterSlice';
import Counter from '../Counter/Counter';

const Counter2 = () => {
  const [{ counter: counter2 }, counter2Dispatch] =
    contextReduxStore.useStore();
  return (
    <Counter
      title={'Context Redux Store Example'}
      data={counter2}
      increase={() => counter2Dispatch(actions.change(-1))}
      reset={() => counter2Dispatch(actions.reset)}
      decrease={() => counter2Dispatch(actions.change(-1))}
    />
  );
};

export default Counter2;
