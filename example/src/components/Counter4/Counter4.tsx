import React from 'react';
import { contextCombineStore } from '../../App';
import { actions } from '../../store/counterSlice';
import Counter from '../Counter/Counter';

const Counter4 = () => {
  const { counter4Store } = contextCombineStore.useStore();
  const [{ counter: counter4 }, counter4Dispatch] = counter4Store;
  return (
    <Counter
      title={'Context Combine Store Example 2'}
      data={counter4}
      increase={() => counter4Dispatch(actions.change(-1))}
      reset={() => counter4Dispatch(actions.reset)}
      decrease={() => counter4Dispatch(actions.change(-1))}
    />
  );
};

export default Counter4;
