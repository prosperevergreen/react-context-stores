import React from 'react';

interface Props {
  title: string;
  data: number;
  increase: () => void;
  reset: () => void;
  decrease: () => void;
}

const Counter = ({ title, data, increase, decrease, reset }: Props) => {
  return (
    <div>
      <h3>{title}</h3>
      <div>{data}</div>
      <div>
        <button onClick={decrease}>-</button>
        <button onClick={reset}>reset</button>
        <button onClick={increase}>+</button>
      </div>
    </div>
  );
};

export default Counter;
