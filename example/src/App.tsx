import React, { useState } from 'react'
import {
  ContextStore,
  ContextReduxStore,
  ContextCombineStore
} from 'react-context-stores'
import * as CounterSetup from './store/counterSlice'
import combineCounterSetup from './store'
import Counter1 from './components/Counter1/Counter1'
import Counter2 from './components/Counter2/Counter2'
import Counter3 from './components/Counter3/Counter3'
import Counter4 from './components/Counter4/Counter4'

export const contextStore = new ContextStore<{
  counter1: number
  setCounter1: React.Dispatch<React.SetStateAction<number>>
}>('Context Store')

export const contextReduxStore = new ContextReduxStore<
  typeof CounterSetup.initialState,
  CounterSetup.ActionType
>('Context Redux Store')

export const contextCombineStore = new ContextCombineStore<
  typeof CounterSetup.initialState,
  CounterSetup.ActionType
>('Context Combine Store')

const App = () => {
  const [counter1, setCounter1] = useState(0)
  return (
    <div>
      <h1>custome-react-store Examples</h1>
      <ul>
        <li>
          <contextStore.Provider value={{ counter1, setCounter1 }}>
            <Counter1 />
          </contextStore.Provider>
        </li>
        <li>
          <contextReduxStore.Provider
            value={[CounterSetup.initialState, CounterSetup.reducer]}
          >
            <Counter2 />
          </contextReduxStore.Provider>
        </li>
        <contextCombineStore.Provider value={combineCounterSetup}>
          <li>
            <Counter3 />
          </li>
          <li>
            <Counter4 />
          </li>
        </contextCombineStore.Provider>
      </ul>
    </div>
  )
}

export default App
